import React from 'react';

// Stateless functional component without "return" call.  Works only when you have one element
const Titles = () => (
  <div>
    <h1 className="title-container__title">Weather Fidner</h1>
    <h3 className="title-container__subtitle">Find out temperature, conditions and more...</h3> 
  </div>
);

export default Titles;