import React from 'react';

// Stateless functional component without "return" call.  Works only when you have one element
const Form = (props) => (
  <form onSubmit={props.getWeather}>
    <input type="text" name="city" placeholder="City..."/>
    <input type="text" name="country" placeholder="Country..."/>
    <button>Get weather</button>
  </form>
);

export default Form;